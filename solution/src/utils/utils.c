#include "../../include/utils/utils.h"

#include <string.h>


uint8_t get_file_format(const char* const path, enum image_format* const format) {
    char* ext = strrchr(path, '.');

    if(!ext) return 1;
    ext++;

    if (!strcmp(ext, image_format_name(IMG_FORMAT_BMP))) {
        *format = IMG_FORMAT_BMP;
    } else if (!strcmp(ext, image_format_name(IMG_FORMAT_SVG))) {
        *format = IMG_FORMAT_SVG;
    } else if (!strcmp(ext, image_format_name(IMG_FORMAT_JPG))) {
        *format = IMG_FORMAT_JPG;
    } else {
        return 1;
    }

    return 0;
}
