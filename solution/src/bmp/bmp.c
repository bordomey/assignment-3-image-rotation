#include "../../include/bmp/bmp.h"
#include "../../include/image_io/image_worker.h"

enum bmp_statusr from_bmp( FILE* const dest, struct image* const img ) {
    if (!dest || !img) return BMP_R_NULL_IMG;

    // Read header
    struct bmp_header header;
    enum bmp_statusr header_read_status = read_header(dest, &header);

    if (header_read_status > 0) return header_read_status;

    // Create image
    *img = create_image(header.biWidth, header.biHeight);

    if (!img->data) return BMP_R_OUT_OF_MEMORY;

    // Read image data
    enum bmp_statusr image_statusr = read_raw_image(dest, img);
    if (image_statusr > 0) destroy_image(img);
    return image_statusr;
}


enum bmp_statusw to_bmp(FILE* const dest, const struct image* const img ) {
    if (!dest || !img) return BMP_W_NULL_IMG;
    enum bmp_statusw header_write_status = write_header(dest, img);
    if (header_write_status > 0) return header_write_status;
    return write_raw_image(dest, img);
}
