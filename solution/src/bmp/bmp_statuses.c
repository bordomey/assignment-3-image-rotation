#include "../../include/bmp/bmp_statuses.h"


char* bmp_rstatus_msg(const enum bmp_statusr res) {
    char* RSTATUS_MSG[BMP_RSTATUS_AMOUNT] = {
            "BMP read is OK",
            "HEADER: Invalid signature",
            "HEADER: Invalid bits",
            "HEADER: Invalid header",
            "IMG: Image is NULL",
            "Memory exceed"
    };

    if (res >= BMP_RSTATUS_AMOUNT)
        return "BMP read error";
    return RSTATUS_MSG[res];
}


char* bmp_wstatus_msg(const enum bmp_statusw res) {
    char* WSTATUS_MSG[BMP_WSTATUS_AMOUNT] = {
            "BMP write is OK",
            "IMG: image is NULL",
            "IMG: image size out of bounds",
            "IO error"
    };

    if (res >= BMP_WSTATUS_AMOUNT)
        return "BMP write error";

    return WSTATUS_MSG[res];
}
