#include "../../include/bmp/bmp_io.h"


static uint8_t get_padding(const uint64_t width, const uint8_t pixel_size) {
    return (IMG_PADDING_BYTES - width * pixel_size % IMG_PADDING_BYTES) % IMG_PADDING_BYTES;
}

static uint32_t calc_file_size(const size_t width, const size_t height) {
    size_t padding = (size_t) get_padding(width, sizeof(struct pixel));

    size_t size = sizeof(struct bmp_header) + width * height * sizeof(struct pixel) + padding * height;

    if(size > UINT32_MAX) return 0;

    return size;
}

// R

enum bmp_statusr read_header(FILE* const dest, struct bmp_header* const header) {
    if(fread(header, sizeof(struct bmp_header), 1, dest) != 1)
        return BMP_R_INVALID_HEADER;
    if(header->bfType != BMP_FILE_SIGNATURE_LE && header->bfType != BMP_FILE_SIGNATURE_BE)
        return BMP_R_INVALID_SIGNATURE;
    if(header->biBitCount != BMP_BITS_COUNT)
        return BMP_R_INVALID_BITS;
    if(fseek(dest, (long) header->bOffBits, SEEK_SET) != 0)
        return BMP_R_INVALID_BITS;
    return BMP_R_OK;
}

enum bmp_statusr read_raw_image(FILE* const dest, struct image* const img ) {
    uint8_t img_padding = get_padding(img->width, sizeof(struct pixel));
    for(uint32_t i = 0; i < img->height; i++) {
        if(fread(img->data + i * img->width, sizeof(struct pixel), img->width, dest) != img->width) {
            return BMP_R_INVALID_BITS;
        }
        if(fseek(dest, img_padding, SEEK_CUR) != 0) {
            return BMP_R_INVALID_BITS;
        }
    }

    return BMP_R_OK;
}

// W

enum bmp_statusw write_header(FILE* const dest, const struct image* const img) {
    uint32_t size = calc_file_size(img->width, img->height);

    if (!size || img->width > UINT32_MAX || img->height > UINT32_MAX) return BMP_W_TOO_BIG;

    // Create bmp header
    struct bmp_header header = (struct bmp_header) {
            .bfType = BMP_FILE_SIGNATURE_LE,
            .bfileSize = size,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_BI_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BMP_BI_PLANES,
            .biBitCount = BMP_BITS_COUNT,
            .biSizeImage = sizeof(struct bmp_header) - size
    };

    // Write header
    if (fwrite(&header, sizeof(struct bmp_header), 1, dest) != 1)
        return BMP_W_ERROR;

    return BMP_W_OK;
}

enum bmp_statusw write_raw_image(FILE* const dest, const struct image* const img) {
    uint8_t img_padding = get_padding(img->width, sizeof(struct pixel));

    // Write image
    for (int i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), (size_t) img->width, dest) != img->width) {
            return BMP_W_ERROR;
        }
        if (fseek(dest, img_padding, SEEK_CUR) != 0) return BMP_W_ERROR;
    }

    return BMP_W_OK;
}
