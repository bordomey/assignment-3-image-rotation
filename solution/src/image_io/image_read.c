#include "../../include/image_io/image_read.h"
#include "../../include/bmp/bmp.h"
#include "../../include/utils/utils.h"

#include <errno.h>


enum statusr read_image(FILE* const in, const enum image_format format, struct image* const img) {
    if (!in) return R_NULL_FILE_POINTER;

    // Check formats
    if (format == IMG_FORMAT_BMP) {
        const enum bmp_statusr statusr = from_bmp(in, img);

        if (statusr > 0) {
            fprintf(stderr, "BMP Read error: %s\n", bmp_rstatus_msg(statusr));

            return R_WRONG_CONTENT;
        }
    } else if (format == IMG_FORMAT_SVG || format == IMG_FORMAT_JPG) {
        return R_UNSUPPORTED_YET_TYPE;
    } else {
        return R_UNSUPPORTED_TYPE;
    }

    return R_OK;
}


enum statusr read_image_from(const char* const path, struct image* const img) {
    if (!path) return R_NULL_PATH;

    // Read file format
    enum image_format format;
    if (get_file_format(path, &format) > 0) return R_UNSUPPORTED_TYPE;

    // Open file
    FILE* in = fopen(path, "rb");
    if (!in) return errno_to_read_status(errno);

    // Read image
    enum statusr res = read_image(in, format, img);

    fclose(in);
    return res;
}
