#include "../../include/image_io/image_io_statuses.h"
#include <errno.h>

char* read_status_message(const enum statusr status) {
    char * R_STATUS_ERRS[R_STATUSES_NUMBER] = {
            "Read is OK",
            "Wrong image type!",
            "Format unsupported",
            "Parsing exception",
            "NULL file pointer exception",
            "no access",
            "path is empty",
            "bad file descriptor",
            "size is out of bounds",
            "not a file",
            "no such file",
            "Read failed"
    };

    if(status >= R_STATUSES_NUMBER)
        return "Read error";

    return R_STATUS_ERRS[status];
}

enum statusr errno_to_read_status(const int error_number) {
    switch (error_number) {
        case EACCES:
            return R_NO_ACCESS;
        case EBADF:
            return R_BAD_DESCRIPTOR;
        case EFBIG:
            return R_FILE_BIG;
        case EISDIR:
            return R_IS_DIR;
        case ENOENT:
            return R_FILE_NOT_FOUND;
        default:
            return R_FILE_ERROR;
    }
}

char* write_status_message(const enum statusw status) {
    char * W_STATUS_ERRS[W_STATUSES_NUMBER] = {
            "Successfully write",
            "Wrong image type",
            "Format not supported",
            "File psarsing exception",
              "NULL file pointer exception",
            "no access",
            "path is empty",
            "bad file descriptor",
            "size is out of bounds",
            "not a file",
            "no such file",
            "Write failed"
    };

    if(status >= W_STATUSES_NUMBER)
        return "Write error";

    return W_STATUS_ERRS[status];
}

enum statusw errno_to_write_status(const int error_number) {
    switch (error_number) {
        case EACCES:
            return W_NO_ACCESS;
        case EBADF:
            return W_BAD_DESCRIPTOR;
        case EFBIG:
            return W_FILE_BIG;
        case EISDIR:
            return W_IS_DIR;
        default:
            return W_FILE_ERROR;
    }
}
