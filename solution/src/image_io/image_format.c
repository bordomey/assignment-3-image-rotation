# include "../../include/image_io/image_format.h"

#include <stddef.h>

char* image_format_name(const enum image_format format) {
    char* IMG_FORMATS_NAMES[IMG_FORMAT_NUMBER] = {
            "bmp",
            "svg",
            "jpg"
    };

    if(format >= IMG_FORMAT_NUMBER) return NULL;

    return IMG_FORMATS_NAMES[format];
}
