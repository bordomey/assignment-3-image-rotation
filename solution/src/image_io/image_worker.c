#include "../../include/image_io/image_worker.h"

struct image create_image(uint64_t width, uint64_t height) {
    struct pixel* image_data = (struct pixel*) malloc(sizeof(struct pixel)*width*height);

    return (struct image) {.width = width, .height = height, .data = image_data};
}


void destroy_image(struct image* image) {
    if (image) {
        free(image->data);
        image->data = NULL;
    }
}
