#include "../../include/image_io/image_write.h"
#include "../../include/bmp/bmp.h"
#include "../../include/utils/utils.h"

#include <errno.h>


enum statusw write_image(FILE* const out, const enum image_format format, const struct image* const img) {
    if(!out) return W_NULL_FILE_POINTER;

    // Check formats
    if (format == IMG_FORMAT_BMP) {
        const enum bmp_statusw statusw = to_bmp(out, img);

        if (statusw > 0) {
            fprintf(stderr, "BMP Write error: %s\n", bmp_wstatus_msg(statusw));

            return W_WRONG_CONTENT;
        }
    } else if (format == IMG_FORMAT_SVG || format == IMG_FORMAT_JPG) {
        return W_UNSUPPORTED_YET_TYPE;
    } else {
        return W_UNSUPPORTED_TYPE;
    }

    return W_OK;
}


enum statusw write_image_to(const char* const path, const struct image* const img) {
    if (!path) return W_NULL_PATH;

    // Read file format
    enum image_format format;
    if (get_file_format(path, &format) > 0) return W_UNSUPPORTED_TYPE;

    FILE* out = fopen(path, "wb+");
    if(!out) return errno_to_write_status(errno);

    enum statusw res = write_image(out, format, img);

    fclose(out);
    return res;
}
