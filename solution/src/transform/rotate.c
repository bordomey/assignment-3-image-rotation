#include "../../include/transform/rotate.h"
#include "../../include/image_io/image_worker.h"


struct image rotate(const struct image image) {
    struct image res = create_image(image.height, image.width);
    if (!res.data) return res;

    for(int i = 0; i < image.height; i++) {
        for(int j = 0; j < image.width; j++) {
            res.data[(j + 1) * image.height - (i + 1)] = image.data[  j + i * image.width];
        }
    }

    return res;
}
