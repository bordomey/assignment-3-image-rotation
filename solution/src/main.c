#include <stdio.h>

#include "../include/transform/rotate.h"

#include "../include/image_io/image_io.h"
#include "../include/image_io/image_worker.h"

int main( int argc, char** argv ) {
    if (argc < 3) {
        if (argc == 1)
            printf("first arg: input file, second: output file");
        if (argc == 2)
            printf("second argument must be output file");

        return 1;
    }

    struct image image;

    const enum statusr statusr = read_image_from(argv[1], &image);

    if(statusr > 0) {
        fprintf(stderr, "%s\n", read_status_message(statusr));
        return statusr;
    }

    struct image result = rotate(image);

    const enum statusw statusw = result.data ?
            write_image_to(argv[2], &result) : W_MEMORY_NOT_ALLOCATED;
    destroy_image(&image);
    destroy_image(&result);

    if(statusw > 0) {
        fprintf(stderr, "%s\n", write_status_message(statusw));
        return statusw;
    }

    return 0;
}
