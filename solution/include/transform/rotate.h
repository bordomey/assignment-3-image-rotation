#ifndef IMG_TRANSFORMER_ROTATE_H
#define IMG_TRANSFORMER_ROTATE_H

#include "../image.h"

struct image rotate(struct image image);

#endif //IMG_TRANSFORMER_ROTATE_H
