#ifndef IMG_TRANSFORMER_UTILS_H
#define IMG_TRANSFORMER_UTILS_H

#include "../image_io/image_format.h"

#include <stdint.h>

uint8_t get_file_format(const char* path, enum image_format* format);

#endif //IMG_TRANSFORMER_UTILS_H
