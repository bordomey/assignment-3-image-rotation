#ifndef IMG_TRANSFORMER_IMG_IO_STATUSES_H
#define IMG_TRANSFORMER_IMG_IO_STATUSES_H

enum statusr {
    R_OK = 0,

    R_UNSUPPORTED_TYPE,
    R_UNSUPPORTED_YET_TYPE,
    R_WRONG_CONTENT,
    R_NULL_FILE_POINTER,

    R_NO_ACCESS,
    R_NULL_PATH,
    R_BAD_DESCRIPTOR,
    R_FILE_BIG,
    R_IS_DIR,
    R_FILE_NOT_FOUND,

    R_FILE_ERROR,

    R_STATUSES_NUMBER  // must be the last
};

char* read_status_message(enum statusr status);
enum statusr errno_to_read_status(int error_number);

enum statusw {
    W_OK = 0,

    W_UNSUPPORTED_TYPE,
    W_UNSUPPORTED_YET_TYPE,
    W_WRONG_CONTENT,
    W_NULL_FILE_POINTER,
    W_MEMORY_NOT_ALLOCATED,

    W_NO_ACCESS,
    W_NULL_PATH,
    W_BAD_DESCRIPTOR,
    W_FILE_BIG,
    W_IS_DIR,

    W_FILE_ERROR,

    W_STATUSES_NUMBER  // must be the last
};

char* write_status_message(enum statusw status);
enum statusw errno_to_write_status(int error_number);

#endif //IMG_TRANSFORMER_IMG_IO_STATUSES_H
