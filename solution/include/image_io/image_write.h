#ifndef IMG_TRANSFORMER_IMG_W_H
#define IMG_TRANSFORMER_IMG_W_H

#include <stdio.h>

#include "../image.h"

#include "image_format.h"
#include "image_io_statuses.h"

enum statusw write_image(FILE* out, enum image_format format, const struct image* img);

enum statusw write_image_to(const char* path, const struct image* img);

#endif //IMG_TRANSFORMER_IMG_W_H
