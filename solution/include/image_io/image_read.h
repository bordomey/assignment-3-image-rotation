#ifndef IMG_TRANSFORMER_IMG_R_H
#define IMG_TRANSFORMER_IMG_R_H

#include <stdio.h>

#include "../image.h"

#include "image_format.h"
#include "image_io_statuses.h"


enum statusr read_image(FILE* in, enum image_format format, struct image* img);

enum statusr read_image_from(const char* path, struct image* img);

#endif //IMG_TRANSFORMER_IMG_R_H
