#ifndef IMG_TRANSFORMER_IMG_WORKER_H
#define IMG_TRANSFORMER_IMG_WORKER_H

#include "../image.h"

#include <stdlib.h>

struct image create_image(uint64_t width, uint64_t height);

void destroy_image(struct image* image);

#endif //IMG_TRANSFORMER_IMG_WORKER_H
