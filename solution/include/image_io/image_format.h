#ifndef IMG_TRANSFORMER_IMG_FORMAT_H
#define IMG_TRANSFORMER_IMG_FORMAT_H

enum image_format {
    IMG_FORMAT_BMP = 0,
    IMG_FORMAT_SVG,
    IMG_FORMAT_JPG,
    // ...
    IMG_FORMAT_NUMBER
};

char* image_format_name(enum image_format format);

#endif //IMG_TRANSFORMER_IMG_FORMAT_H
