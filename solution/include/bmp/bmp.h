#ifndef IMG_TRANSFORMER_BMP_H
#define IMG_TRANSFORMER_BMP_H

#include "bmp_io.h"

enum bmp_statusr from_bmp(FILE* dest, struct image* img);

enum bmp_statusw to_bmp(FILE* dest, const struct image* img);

#endif //IMG_TRANSFORMER_BMP_H
