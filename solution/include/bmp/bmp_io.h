#ifndef IMG_TRANSFORMER_BMP_IO_H
#define IMG_TRANSFORMER_BMP_IO_H

#include <stdint.h>
#include <stdio.h>

#include "../image.h"

#include "bmp_header_format.h"
#include "bmp_statuses.h"

#define BMP_FILE_SIGNATURE_LE 0x4D42
#define BMP_FILE_SIGNATURE_BE 0x422D
#define BMP_BI_PLANES 1
#define BMP_BI_SIZE 40
#define BMP_BITS_COUNT 24
#define IMG_PADDING_BYTES 4

enum bmp_statusr read_header(FILE* dest, struct bmp_header* header);

enum bmp_statusr read_raw_image(FILE* dest, struct image* img );

enum bmp_statusw write_header(FILE* dest, const struct image* img);

enum bmp_statusw write_raw_image(FILE* dest, const struct image* img);

#endif //IMG_TRANSFORMER_BMP_IO_H
