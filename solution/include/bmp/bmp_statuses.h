#ifndef IMG_TRANSFORMER_BMP_STATUSES_H
#define IMG_TRANSFORMER_BMP_STATUSES_H

/*  deserializer   */
enum bmp_statusr  {
    BMP_R_OK = 0,
    BMP_R_INVALID_SIGNATURE,
    BMP_R_INVALID_BITS,
    BMP_R_INVALID_HEADER,
    BMP_R_NULL_IMG,
    BMP_R_OUT_OF_MEMORY,
    BMP_RSTATUS_AMOUNT  // must be the last
};

char* bmp_rstatus_msg(enum bmp_statusr res);

/*  serializer   */
enum bmp_statusw  {
    BMP_W_OK = 0,
    BMP_W_NULL_IMG,
    BMP_W_TOO_BIG,
    BMP_W_ERROR,
    BMP_WSTATUS_AMOUNT  // must be the last
};

char* bmp_wstatus_msg(enum bmp_statusw res);

#endif //IMG_TRANSFORMER_BMP_STATUSES_H
